#!/usr/bin/env python
import mincemeat
import glob
import os

#If All_files is true. The script will get all files in path
ALL_FILES = True
DIR_FILES = '/hw3data'

PROJECT_DIR = os.path.dirname(os.path.dirname(__file__))
#PROJECT_DIR = os.getcwd()
#PROJECT_DIR = os.path.dirname(os.path.dirname(arquivo))


DATA_FILES = PROJECT_DIR + DIR_FILES

if(ALL_FILES):
    DATA_FILES = DATA_FILES + '/*'
else:
    DATA_FILES = DATA_FILES + '/c0001'


text_files = glob.glob(DATA_FILES)

#Read All Files in Data Dir
data = []
for file_name in text_files:
    f = open(file_name)
    try:
        data += f.read().splitlines()
    finally:
        f.close()

print 'Data will process: %d in %d files' % (len(data), len(text_files))

# The data source can be any dictionary-like object
datasource = dict(enumerate(data))

def mapfn(k, v):
    import stopwords
    print "%d => %s" % (k,v)
    line = v.split(":::")
    paperid=line[0]
    authors=line[1].split("::")
    title=str(line[2]).replace("."," ").replace(","," ").replace("?","")

    dict_word_title = {}
    for word in title.split():
        slug_word = word.replace("-"," ").lower().strip()
        if len(slug_word ) > 1 and slug_word != '' and not slug_word in  stopwords.allStopWords:
            dict_word_title[slug_word] = dict_word_title.get(slug_word,0) +1

    for author in authors:
        yield author.lower(), {'paperid': paperid, 'dict_word_title' : dict_word_title}

def reducefn(k, vs):
    full_dict_word = {}
    for paper in vs:
        for word in paper['dict_word_title']:
            full_dict_word[word] = full_dict_word.get(word,0) + 1

    return full_dict_word

s = mincemeat.Server()
s.datasource = datasource
s.mapfn = mapfn
s.reducefn = reducefn

results = s.run_server(password="changeme")

AuthorList = list(results)
AuthorList.sort()

file_name = "output.txt"
file = open(file_name, "w+")


try:
    for author in AuthorList:
        print author
        file.write(author+"\n")
        final_word_list = list(results[author])
        final_word_list.sort()
        res = list(sorted(results[author], key=results[author].__getitem__, reverse=True))
        #Only 2 top words
        for word in res[:2]:
            will_print = "   (%d) - %s " % (results[author][word], word)
            print will_print
            file.write(will_print+"\n")
finally:
    file.close()